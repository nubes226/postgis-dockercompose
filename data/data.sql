--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12
-- Dumped by pg_dump version 12.12

-- Started on 2023-04-05 14:31:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 252 (class 1259 OID 103580)
-- Name: structure; Type: TABLE; Schema: public; Owner: contravention
--

CREATE TABLE public.structure (
    id bigint NOT NULL,
    denomination character varying(255),
    sigle character varying(255),
    contact character varying(255),
    grade_id bigint
);


ALTER TABLE public.structure OWNER TO hsawadogo;

--
-- TOC entry 2993 (class 0 OID 103580)
-- Dependencies: 252
-- Data for Name: structure; Type: TABLE DATA; Schema: public; Owner: contravention
--

INSERT INTO public.structure (id, denomination, sigle, contact, grade_id) VALUES (5, 'POLICE NATIONALE', 'PN', '123', NULL);
INSERT INTO public.structure (id, denomination, sigle, contact, grade_id) VALUES (6, 'GENDARMERIE NATIONALE', 'GN', '124', NULL);
INSERT INTO public.structure (id, denomination, sigle, contact, grade_id) VALUES (3851, 'Agence Nationale de Promotion de TICs', 'ANPTIC', '65478962', NULL);


--
-- TOC entry 2865 (class 2606 OID 103626)
-- Name: structure structure_pkey; Type: CONSTRAINT; Schema: public; Owner: contravention
--

ALTER TABLE ONLY public.structure
    ADD CONSTRAINT structure_pkey PRIMARY KEY (id);
